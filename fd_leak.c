// Copyright © 2015-2016 Martin Ueding <dev@martin-ueding.de>

// Try to leak as many file descriptors until the program crashes.

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    char filename[100];

    for (size_t i = 0; i < 1500; ++i) {
        sprintf(filename, "/tmp/%010ld.txt", i);
        FILE *fp = fopen(filename, "w");

        if (fp == NULL) {
            printf("i = %6ld: failed!\n", i);
            return 1;
        }

        if (i % 100 == 0) {
            printf("i = %6ld: ok.\n", i);
        }
    }

    return 0;
}